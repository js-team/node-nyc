Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: nyc
Upstream-Contact: https://github.com/istanbuljs/nyc/issues
Source: https://github.com/istanbuljs/nyc#readme

Files: *
Copyright: 2015, Contributors
License: ISC

Files: append-transform/*
 caching-transform/*
Copyright: James Talmage <james@talmage.io>
License: Expat

Files: cp-file/*
 hasha/*
 p-event/*
 type-fest/*
Copyright: Sindre Sorhus <sindresorhus@gmail.com>
License: Expat

Files: default-require-extensions/*
Copyright: Node.js contributors
 James Talmage <james@talmage.io>
License: Expat

Files: fromentries/*
Copyright: Feross Aboukhadijeh
License: Expat

Files: html-escaper/
Copyright: 2017-present, Andrea Giammarchi
License: Expat

Files: istanbul-lib-coverage/*
 istanbul-lib-hook/*
 istanbul-lib-instrument/*
 istanbul-lib-processinfo/*
 istanbul-lib-report/*
 istanbul-lib-source-maps/*
 istanbul-reports/*
Copyright: 2012-2015, Yahoo! Inc.
License: BSD-3-Clause

Files: istanbuljsload-nyc-config/*
Copyright: 2019, Contributors
License: ISC

Files: istanbuljsschema/*
 node-preload/*
 process-on-spawn/*
Copyright: 2019, CFWare, LLC
License: Expat

Files: merge-source-map/*
Copyright: keik <k4t0.kei@gmail.com>
License: Expat

Files: nested-error-stacks/*
Copyright: 2014, Matt Lavin <matt.lavin@gmail.com>
License: Expat

Files: package-hash/*
 release-zalgo/*
Copyright: 2016-2017, Mark Wubben <mark@novemberborn.net>
License: ISC

Files: spawn-wrap/*
Copyright: Isaac Z. Schlueter and Contributors
License: ISC

Files: test-exclude/*
Copyright: 2016, Contributors
License: Expat

Files: debian/*
Copyright: 2019, Yadd <yadd@debian.org>
License: ISC

Files: debian/tests/test_modules/any-path/*
Copyright: 2015, Contributors
License: ISC
Comment: The upstream distribution does not contain an explicit statement of
 copyright ownership. Pursuant to the Berne Convention for the Protection of
 Literary and Artistic Works, it is assumed that all content is copyright by
 its respective authors unless otherwise stated.

Files: debian/tests/test_modules/esm/*
Copyright: esm contributors
License: Expat
Comment: Based on reify, copyright Ben Newman <https://github.com/benjamn/reify>

Files: debian/tests/test_modules/fromentries/*
Copyright: Feross Aboukhadijeh
License: Expat

Files: debian/tests/test_modules/html-escaper/*
Copyright: 2017-present, Andrea Giammarchi
License: Expat

Files: debian/tests/test_modules/ts-node/*
Copyright: 2014, Blake Embrey <hello@blakeembrey.com>
License: Expat

License: BSD-3-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
     * Redistributions of source code must retain the above copyright
       notice, this list of conditions and the following disclaimer.
     * Redistributions in binary form must reproduce the above copyright
       notice, this list of conditions and the following disclaimer in the
       documentation and/or other materials provided with the distribution.
     * Neither the name of the Yahoo! Inc. nor the
       names of its contributors may be used to endorse or promote products
       derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL YAHOO! INC. BE LIABLE FOR ANY
 DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

License: ISC
 Permission to use, copy, modify, and/or distribute this software for any
 purpose with or without fee is hereby granted, provided that the above
 copyright notice and this permission notice appear in all copies.
 .
 THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
